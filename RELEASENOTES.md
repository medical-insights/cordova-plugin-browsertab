### 0.2.1
* Add support for `CUSTOM_TAB_COLOR_RGB` parameter to customize browser tab background (only Android supported at the moment)
### 0.2.2
* Fixed Android build